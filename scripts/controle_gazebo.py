#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 28 14:17:07 2018

@author: bruno
"""

import rospy
from geometry_msgs.msg import Pose2D
from geometry_msgs.msg import Twist


class PID:


	def __init__(self, P=0.05, I=0.0, D=0.0, Derivator=0, Integrator=0, Integrator_max=1, Integrator_min=-1, PID_max=1, PID_min=-1):

		self.Kp=P
		self.Ki=I
		self.Kd=D
		self.Derivator=Derivator
		self.Integrator=Integrator
		self.Integrator_max=Integrator_max
		self.Integrator_min=Integrator_min
		self.PID_max=PID_max
		self.PID_min=PID_min

		self.set_point=0.0
		self.error=0.0

	def update(self,current_value):
		"""
		Calculate PID output value for given reference input and feedback
		"""

		self.error = self.set_point - current_value

		self.P_value = self.Kp * self.error
		self.D_value = self.Kd * ( self.error - self.Derivator)
		self.Derivator = self.error

		self.Integrator = self.Integrator + self.error

		if self.Integrator > self.Integrator_max:
			self.Integrator = self.Integrator_max
		elif self.Integrator < self.Integrator_min:
			self.Integrator = self.Integrator_min

		self.I_value = self.Integrator * self.Ki

		PID = self.P_value + self.I_value + self.D_value
		
		if PID> self.PID_max:
			PID = self.PID_max
		elif PID< self.PID_min:
			PID = self.PID_min
	
		return PID

	def setPoint(self,set_point):
		"""
		Initilize the setpoint of PID
		"""
		self.set_point = set_point
		self.Integrator=0
		self.Derivator=0

	def setIntegrator(self, Integrator):

		self.Integrator = Integrator
	def setDerivator(self, Derivator):
		self.Derivator = Derivator

	def setKp(self,P):
		self.Kp=P

	def setKi(self,I):
		self.Ki=I

	def setKd(self,D):
		self.Kd=D

	def getPoint(self):
		return self.set_point

	def getError(self):
		return self.error

	def getIntegrator(self):
		return self.Integrator

	def getDerivator(self):
		return self.Derivator

	def setMax(self,Ma):
		self.PID_max = Ma
	
	def setMin(self,Mi):
		self.PID_min = Mi
    
def send(linear, angular):
    
     pub = rospy.Publisher('/pioneer2dx/cmd_vel', Twist, queue_size=10)
     velocities = Twist()
     velocities.linear.x = linear
     velocities.linear.y = 0
     velocities.linear.z = 0
     velocities.angular.x = 0
     velocities.angular.y = 0
     velocities.angular.z = angular
     pub.publish(velocities)

     
def process(erro):

    vlin=0;
    vtheta=0;
    
    if abs(erro.theta)>20:
#        Kphi = 0.4
#        vtheta = Kphi*erro.theta
#        vlin =0
        
         if erro.theta > 0:
             vtheta = -1
         else:
             vtheta = 1        
        
    elif abs(erro.x) > 20:
        Kv = 0.005
        vlin = Kv*erro.x
        vtheta = 0

         
        
 
 
# 
# 
#    if abs(erro.theta)>20:
#         vlin=0
#
#          
#         if erro.theta>0:
#             vtheta = -1
#         else:
#             vtheta = 1
#        
#    elif abs(erro.theta) <20 and abs(erro.x) > 20:
#         vtheta=0
#         vlin = 0.5

#
    send(vlin,vtheta)
    print "lin: ", round(vlin,2), "ang: ", round(vtheta,2)
        #rospy.loginfo(str(w)+str(v))
        

def receive():

	rospy.init_node('controle_gazebo', anonymous=True)


	rospy.Subscriber("ToController", Pose2D, process)
	


	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()

if __name__ == '__main__':
	receive()
