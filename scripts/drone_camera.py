#!/usr/bin/env python
import roslib; roslib.load_manifest('drone_tg')
import rospy

from std_srvs.srv import Empty
# Import the two types of messages we're interested in
from sensor_msgs.msg import Image         # for receiving the video feed
from ardrone_autonomy.msg import Navdata # for receiving navdata feedback
# We need to use resource locking to handle synchronization between GUI thread and ROS topic callbacks
from threading import Lock
# An enumeration of Drone Statuses
from drone_status import DroneStatus
#Para o Subscriber da camera:
from sensor_msgs.msg import CameraInfo
# We need an image converted to use opencv
import cv2
from image_converter import ToOpenCV, ToRos

#Adicionado para processamento de imagem:
from collections import deque
import numpy as np
import imutils
import time

#biblioteca para tarbahar com tags aruco
import cv2.aruco as aruco

import math
import datetime

# Some Constants
CONNECTION_CHECK_PERIOD = 2.250 #seconds
GUI_UPDATE_PERIOD = 0.0333 #30 fps -> limite da camera frontal



##### TODOS OS CODIGOS PARA OPTICAL FLOW vvvvvvvvvvvvvvvvvvvvvvvvvvv
# params for ShiTomasi corner detection
feature_params = dict( maxCorners = 50,
                       qualityLevel = 0.70,
                       minDistance = 1,
                       blockSize = 7)

# Parameters for lucas kanade optical flow
lk_params = dict( winSize  = (15,15),
                  maxLevel = 2,
                  criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))
                  
# Create some random colors
color = np.random.randint(0,255,(100,3))
old_gray = cv2.cvtColor(np.zeros((400,600,3), np.uint8), cv2.COLOR_BGR2GRAY)
p0 = float(0)

mask = 0
a=0
b=0

timeSample = 0
lastTime = 0
mediaTime = np.array([])

conta_aruco = 0
conta_opt = 0


logData= open("/home/sergio/Desktop/" + str(datetime.datetime.time(datetime.datetime.now())) + ".xlsx","w")
logData.write("%s\t%s\t%s\t%s\n" % ("pos x", "pos y","pos theta", "time stamp"))
##### TODOS OS CODIGOS PARA OPTICAL FLOW ^^^^^^^^^^^^^^^^^^^^^^^

class DroneVideoDisplay():
    StatusMessages = {
        DroneStatus.Emergency : 'Emergency',
        DroneStatus.Inited    : 'Initialized',
        DroneStatus.Landed    : 'Landed',
        DroneStatus.Flying    : 'Flying',
        DroneStatus.Hovering  : 'Hovering',
        DroneStatus.Test      : 'Test (?)',
        DroneStatus.TakingOff : 'Taking Off',
        DroneStatus.GotoHover : 'Going to Hover Mode',
        DroneStatus.Landing   : 'Landing',
        DroneStatus.Looping   : 'Looping (?)'
        }
    DisconnectedMessage = 'Disconnected'
    UnknownMessage = 'Unknown Status'


    def __init__(self):

        # Subscribe para as informacoes da camera do drone:
        self.subCamerainfo = rospy.Subscriber('/ardrone/camera_info', CameraInfo, self.Recebecamera)

        # Subscribe para as imagens recebidas do drone.
        self.subVideo   = rospy.Subscriber('/ardrone/image_raw',Image,self.ReceiveImage)

        # Holds the image frame received from the drone and later processed by the GUI
        self.image = None
        self.imageLock = Lock()
        #cv2.namedWindow("windowimage", cv2.WINDOW_NORMAL)
        
        # Holds the status message to be displayed on the next GUI update
        self.statusMessage = ''
        
        self.camera = ""      
        self.flagPrimeiraRecepcao = True
        self.x = 0
        self.y = 0
        self.phi = 90       #inicializa com 90 pois eh valor de erro zero para angulo
        self.erro_ang = 0
        self.erro_lin = 0
        self.radius = 0
        self.flag_decolou = False
        self.flagRecalculaDelta = True
        #self.last_time = time.time()#TRECHO DE CODIGO PARA RECEBER PONTOS A CADA INTERVALO DE TEMPO

        # Tracks whether we have received data since the last connection check
        # This works because data comes in at 50Hz but we're checking for a connection at 4Hz
        self.communicationSinceTimer = False
        self.connected = False

        # A timer to check whether we're still connected
        self.connectionTimer = rospy.Timer(rospy.Duration(CONNECTION_CHECK_PERIOD),self.ConnectionCallback)

        # A timer to redraw the GUI
        self.redrawTimer = rospy.Timer(rospy.Duration(GUI_UPDATE_PERIOD),self.RedrawCallback, oneshot=False)  
        
        # Caso o ROS seja desligado: fecha a janela aberta para visualizacao da camera
        rospy.on_shutdown(self.FechaJanela)
        



            

   # Called every CONNECTION_CHECK_PERIOD ms, if we haven't received anything since the last callback, will assume we are having network troubles and display a message in the status bar
    def ConnectionCallback(self,event):
        self.connected = self.communicationSinceTimer
        self.communicationSinceTimer = False

    def RedrawCallback(self,event):
        global old_gray
        global p0
        global mask
        global a, bgeometry_msgs
        global flag_decolou
        global delta_x, delta_y
        global timeSample, lastTime,mediaTime
        global conta_aruco, conta_opt

        
        timeSample = time.time() - lastTime

#        print "timeSample = ", timeSample
        
        if lastTime != 0:        
            mediaTime = np.append(mediaTime, timeSample) 
#            print "media de Tempo = ", (np.sum(mediaTime)/mediaTime.size)
        lastTime = time.time()      



        #Nao detectou um amuco
        
            #Encontra pontos e faz estabilizacao por optical flow

        #Detectou um aruco
            #Faz controle do drone em funcao do aruco
        
        
#        if ((time.time()-self.last_time)>3):#TRECHO DE CODIGO PARA RECEBER PONTOS A CADA INTERVALO DE TEMPO
#            self.flagPrimeiraRecepcao = True#TRECHO DE CODIGO PARA RECEBER PONTOS A CADA INTERVALO DE TEMPO

        #if para entrar na primeira deteccao
        #verifica se foi primeiro frame que recebeu para extrair goodfeaturetotrack  
        if ((self.image is not None) and (self.flagPrimeiraRecepcao == True) and (self.flag_decolou)):
            self.imageLock.acquire()
            try:

                #na primeira recepcao faz os dois metodos para inicicalizar doas as variavais
                grayaruco = cv2.cvtColor(ToOpenCV(self.image), cv2.COLOR_BGR2GRAY)
                aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_1000)
                parameters =  aruco.DetectorParameters_create()
                corners, ids, rejectedImgPoints = aruco.detectMarkers(grayaruco, aruco_dict, parameters=parameters)                    
                    
                
                #abaxo metodo do optical flow                    
                # Take first frame and find corners in it
                old_frame = ToOpenCV(self.image)
                #old_frame = imutils.resize(image_cv, width=600)
                old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
                p0 = cv2.goodFeaturesToTrack(old_gray, mask = None, **feature_params)
                
                # Create a mask image for drawing purposes            
                mask = np.zeros_like(old_frame)

                #zera flag de primeira recepcao somente quando tiver encontrado good features to track.
                if p0 is not None:
                    self.flagPrimeiraRecepcao = False                  
                
                
                
                
                #abaixo, metodo para aruco marker
                #grayaruco = cv2.cvtColor(ToOpenCV(self.image), cv2.COLOR_BGR2GRAY)
                #aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
                #parameters =  aruco.DetectorParameters_create()
                #corners, ids, rejectedImgPoints = aruco.detectMarkers(grayaruco, aruco_dict, parameters=parameters)
                
                #grayaruco = aruco.drawDetectedMarkers(grayaruco, corners, borderColor = (0, 255, 0))
                #cv2.imshow('aruco',grayaruco)


                    
                     
                
            finally:
                #print "Deu release"
                self.imageLock.release()         
#        elif (self.image is not None) and (self.camera != 'ardrone_base_frontcam'):
        
        elif ((self.image is not None) and (self.flag_decolou)):
            
            
            
            # We have some issues with locking between the display thread and the ros messaging thread due to the size of the image, so we need to lock the resources
            self.imageLock.acquire()
            try:
                
                grayaruco = cv2.cvtColor(ToOpenCV(self.image), cv2.COLOR_BGR2GRAY)
                #print grayaruco.shape
                aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_1000)
                parameters =  aruco.DetectorParameters_create()
                corners, ids, rejectedImgPoints = aruco.detectMarkers(grayaruco, aruco_dict, parameters=parameters)
                
                grayaruco = aruco.drawDetectedMarkers(grayaruco, corners, borderColor = (0, 255, 0))

                
                #se nao tiver corners, faz estabilizacao do voo com good features to track
                if not corners:
               
                    conta_opt = conta_opt+1               
               
                    #convertendo a imagem para o OpenCV
                    frame = ToOpenCV(self.image)
                    #Alterando o tamanho da imagem
                    #frame = imutils.resize(image_cv, width=640)
                    #aplicando um filtro para suavizar a imagem
                    #blurred = cv2.GaussianBlur(frame, (11, 11), 0)
                    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                    # calculate optical flow
                    

    
                    p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)

    
                    if (p1 is not None) and (p0 is not None):
                    
                        good_new = p1[st==1]
                        good_old = p0[st==1]
                        
                        media_x = 0
                        media_y = 0
                        # draw the tracks
                        for i,(new,old) in enumerate(zip(good_new,good_old)):
                            a,b = new.ravel()
                            c,d = old.ravel()
                            mask = cv2.line(mask, (a,b),(c,d), color[i].tolist(), 2)
                            frame = cv2.circle(frame,(a,b),3,color[i].tolist(),-1)
                            
                            if (media_x == 0 and media_y == 0):
                                media_x = a
                                media_y = b
                            else:
                                media_x = (media_x + a) / 2
                                media_y = (media_y + b) / 2
                        
                        frame = cv2.circle(frame,(int(media_x),int(media_y)),5,(0,0,255),-1)
                            
                        #calcular a media dos pontos abc
        
                        
        
                        old_gray = frame_gray.copy()
                        p0 = good_new.reshape(-1,1,2)
                        

                        
                        x_centro = frame.shape[1]/2                        
                        y_centro = frame.shape[0]/2
                        frame = cv2.circle(frame,(int(x_centro),int(y_centro)),6,(255,255,255),-1)
                        
                        if self.flagRecalculaDelta:                        
                            self.flagRecalculaDelta = False                            
                            delta_x = media_x - x_centro
                            delta_y = media_y - y_centro
                            
                            
#                        print "frame = ", frame.shape                        
#                        
#                        print "x_centro = ", x_centro
#                        print "y_centro = ", y_centro                         
#                        
#                        print "delta_x = ", delta_x
#                        print "delta_y = ", delta_y                        
                        
                        
                        x_opt = media_x - delta_x
                        y_opt =media_y - delta_y
                        

#                        print "x_opt = ", x_opt
#                        print "y_opt = ", y_opt
                        
                        frame = cv2.circle(frame,(int(x_opt),int(y_opt)),3,(255,50,50),-1)
                        
                        img = cv2.add(frame,mask)
                        cv2.imshow('Controle',img)
                        
#                        self.x = x_opt
#                        self.y = y_opt
#                        self.phi = 90
                    #se perdeu todos os pontos p0 e p1 precisa reiniciar goodfeaturestotrack
                    else:
                        self.flagPrimeiraRecepcao = True
                        self.flagRecalculaDelta = True
                        
                
                #se tem corners, ou seja se tem tags
                else:
                    
                    conta_aruco = conta_aruco+1               
                    
                    
                    #qual posicao tem id 0
                    if 0 in ids:
                        index_id0 = np.where(ids==0)[0][0]
                        media_id0 = map(sum, map(lambda l: map(float, l), zip(*corners[index_id0][0])))
                        media_id0 = [x / 4 for x in media_id0]
                        
                        
            
                        #detecta os quadrantes
                        dy = corners[index_id0][0][3][1] - corners[index_id0][0][0][1]
                        dx = corners[index_id0][0][3][0] - corners[index_id0][0][0][0]
                       #print "dx: ", dx
                        #print "dy: ", dy
                        
                        #primeiro quadrante        
                        if dy >= 0 and dx <= 0:
                            #print "Q1"
                            phi_carro = math.degrees(math.atan(abs(dy)/abs(dx)))
                   
                        #segundo quadrante
                        elif dy >= 0 and dx >= 0:
                            #print "Q2"
                            phi_carro = 180 - math.degrees(math.atan(abs(dy)/abs(dx)))
                            
                        #terceiro quadrante
                        elif dy <= 0 and dx >= 0: 
                            #print "Q3"
                            phi_carro = math.degrees(math.atan(abs(dy)/abs(dx))) - 180
                            
                        #quarto quadrante
                        elif dy <= 0 and dx <= 0:
                            #print "Q4"
                            phi_carro = -math.degrees(math.atan(abs(dy)/abs(dx)))
            
                        self.x = media_id0[0]
                        self.y = media_id0[1]
                        self.phi = phi_carro
                        logData.write("%d\t%d\t%d\t%f\n" % (self.x, self.y, self.phi, time.time()))

                    else:
                        self.erro_ang = 0
                        self.erro_lin = 0
                        
                        self.flagPrimeiraRecepcao = True #seta flag primeira recepcao para encontrar novos pontos
                        self.flagRecalculaDelta = True
                            
            
                    #verifica se tem tags 1 E 0                    
                    if 1 in ids and 0 in ids:        
                        #qual posicao tem id                        
                        index_id1 = np.where(ids==1)[0][0]
                        #calcula media dos pontos da tag com id 1
                        media_id1 = map(sum, map(lambda l: map(float, l), zip(*corners[index_id1][0])))
                        media_id1 = [x / 4 for x in media_id1]
                        
                        self.erro_lin = math.sqrt(np.power(media_id1[0]-media_id0[0],2) + np.power(media_id1[1]-media_id0[1],2))
                                    
                        cv2.line(grayaruco, tuple(map(int,media_id0)), tuple(map(int,media_id1)),(0,0,255),5)
                        
                        
                        #calcula angulo da reta que liga as duas tag com o eixo x da imagem
                        
                        #detecta os quadrantes
                        dy = media_id1[1] - media_id0[1]
                        dx = media_id1[0] - media_id0[0]
                        
                        #primeiro quadrante        
                        if dy <= 0 and dx >= 0:
                            #print "Q1"
                            phi_reta = math.degrees(math.atan(abs(dy)/abs(dx)))
                   
                        #segundo quadrante
                        elif dy <= 0 and dx < 0:
                            #print "Q2"
                            phi_reta = 180 - math.degrees(math.atan(abs(dy)/abs(dx)))
                            
                        #terceiro quadrante
                        elif dy >= 0 and dx <= 0: 
                            #print "Q3"
                            phi_reta = math.degrees(math.atan(abs(dy)/abs(dx))) - 180
                            
                        #quarto quadrante
                        elif dy >= 0 and dx > 0:
                            #print "Q4"
                            phi_reta = -math.degrees(math.atan(abs(dy)/abs(dx)))
            
                        if phi_reta > 0:
                            self.erro_ang = phi_reta - phi_carro 
                        elif phi_reta < -90:
                            self.erro_ang = 360 + phi_reta - phi_carro 
                        else:
                            self.erro_ang = phi_reta - phi_carro 
                                                
                        
#                        print "carro = ", phi_carro            
                        #print "reta = ", phi_reta
                        #print "erro =  ", self.erro_ang
                    
                    #nao detectou nenhuma tag
                    else:
                        self.erro_ang = 0
                        self.erro_lin = 0
                        
                        self.flagPrimeiraRecepcao = True #seta flag primeira recepcao para encontrar novos pontos
                        self.flagRecalculaDelta = True
                    
                    
                    if 0 in ids:
                        cv2.imshow('Controle',grayaruco)
            finally:
                self.imageLock.release()

            
#                print "% detc. aruco: ", conta_aruco*100//(conta_aruco + conta_opt), "%"
                if conta_aruco > 100:
                   conta_aruco = 0
                   conta_opt = 0


	#Exibindo a imagem do drone na tela do PC:

#            height, width, channels = frame.shape
#            print "heidth", height
#            print "width", width
            #cv2.line(frame,(200,0),(200,400),(255,0,0),5)
            #cv2.line(frame,(400,0),(400,400),(255,0,0),5)
            #cv2.line(frame,(0,337/3),(600,(337/3)),(255,0,0),5)
            #cv2.line(frame,(0,(337*2/3)),(600,(337*2/3)),(255,0,0),5)
            #cv2.imshow("droneview", img)
            #cv2.imshow("hsv", hsv)
            #cv2.imshow("mascara", mask)
            cv2.waitKey(2)

	#Salvando a imagem do robo para posterior calibracao
            #cv2.imwrite('imagem_original.jpeg',image_cv)

    def ReceiveImage(self,data):
        # Indicate that new data has been received (thus we are connected)
        self.communicationSinceTimer = True

        # We have some issues with locking between the GUI update thread and the ROS messaging thread due to the size of the image, so we need to lock the resources
        self.imageLock.acquire()
        try:
            self.image = data # Save the ros image for processing by the display thread
        finally:
            self.imageLock.release()

    #acrescentado para troca de cameras                            

    def trocaCam(self):
        try:
            rospy.wait_for_service('/ardrone/togglecam')
            troca_cam = rospy.ServiceProxy('/ardrone/togglecam', Empty)
            troca_cam()
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    #Receber a informacao de qual camera esta sendo utilizada no momento
    def Recebecamera(self,camera_info):   
        self.camera = camera_info.header.frame_id
    
    def FechaJanela(self):
        cv2.destroyAllWindows()
        logData.close()


