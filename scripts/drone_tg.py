#!/usr/bin/env python

#bilbiotecas
#<--para togglecam-->
import roslib; roslib.load_manifest('drone_tg')
from std_srvs.srv import Empty

#<--para togglecam-->
import rospy
import time
import cv2
import sys
from drone_controller import BasicDroneController
from drone_camera import DroneVideoDisplay
from drone_status import DroneStatus
import curses
#Para comunicacao com carro
from geometry_msgs.msg import Pose2D

# O que deve ser implementado
#			criar media de todos os pontos encontrados na tag para encontrar ponto medio.
#			controle proporcional de velocidade em funcao da distancia do ponto para o centro
#			deteccao de orientacao da tag (isso aqui pode ser dificil, caso trave nisso, utilizar bibliotecas de deteccao automatica de features da tag)
#			utiilzar redes neurais pra detectar padrao da tag e encontra-la antes de encontrar o pontos para fazer tracking
#			implementar controle de altura em z utilizando sensor de altura

if __name__=='__main__':
    
    # Firstly we setup a ros node, so that we can communicate with the other packages
    rospy.init_node('ardrone_drone')
    
    time.sleep(1)

    #Definindo objetos para exibicao da camera na tela do computador e para controle do Drone
    display = DroneVideoDisplay()
    controller = BasicDroneController()
    
    #Publisher para comunicacao com o carro
    pub = rospy.Publisher('ToController', Pose2D, queue_size=10)
    carro = Pose2D()
    carro.x = 0
    carro.y=0
    carro.theta =0
    
    #Iniciando o voo do drone:
    
    #Enquanto o drone nao estiver com seu status como voando ou em Hovering envia sinal para voar
    while (controller.status != DroneStatus.Flying and controller.status != DroneStatus.GotoHover):
#      print "decolando"
      controller.SendTakeoff()
        
#    Subindo o drone para ficar numa altura maior
#    time.sleep(3)
    controller.command.linear.z = 1
    print "subindo"
    time.sleep(14)
    controller.command.linear.z = 0    
   
    #Trocando para a camera traseira para iniciar a busca pelos objetos de interesse:
    if (display.camera == 'ardrone_base_frontcam'):
        display.trocaCam()  
    
    time.sleep(1)    
#       Ligando a flag de que o Drone decolou
    display.flag_decolou =True
    
    time.sleep(1)   
        
    #inicializacao das variaveis de controle
    #Kpx = 1.4 Kix = 0.874 Kdx = 1
    #Kpy = 2.5 kiy = 1.5452 Kdy = 0.39        
    Kpx = 1.4
    Kix = 0.874
    Kdx = 1
    Kpy = 2.5
    Kiy = 1.5452
    Kdy = 0.3937
    INTEGRAL_VALUE = 1
    SCALE = 1
    integralx = 0    
    integraly = 0    
   
#    Kpx = 1
#    Kix = 0
#    Kdx = 0
#    Kpy = 1
#    Kiy = 0
#    Kdy = 0
#    INTEGRAL_VALUE = 1
#    SCALE = 1
#    integralx = 0    
#    integraly = 0     
   
    Kvphi = 0	#Kv e o valor maximo de velocidade que sera aplicado para o valor maximo de erro       

    #pega erro em x
    tam_x = 600		#depois, subistituir esse valor pela variavel de redimensionamento do frame, assim, quando oframe for redimensionado para melhorar a precisao, nao precisao nao precisa mudar essa variavel
    #Encontra o erro em x, que pode variar entre -1 e +1
    last_erro_y = (tam_x/2 - display.x)/(tam_x/2)    
    
    #pega erro em x
    tam_y = 337					#depois, subistituir esse valor pela variavel de redimensionamento do frame, assim, quando oframe for redimensionado para melhorar a precisao, nao precisao nao precisa mudar essa variavel
    #Encontra o erro em x, que pode variar entre -1 e +1
    last_erro_x = (tam_y/2 - display.y)/(tam_y/2)        
    
    last_time = time.time()
    #print "primeiro last_time", last_time
    
    time.sleep(1) 
    
    while 1==1:
      #pega intervalo de tempo entre uma iteracao
      ts = time.time() - last_time
      last_time = time.time()        
        
#----------- PARA X -----------
      #<--Controle do voo do drone-->     
      Kvx = 1  #Kv e o valor maximo de velocidade que sera aplicado para o valor maximo de erro
      tam_x = 600	#depois, subistituir esse valor pela variavel de redimensionamento do frame, assim, quando oframe for redimensionado para melhorar a precisao, nao precisao nao precisa mudar essa variavel
      #Encontra o erro em x, que pode variar entre -1 e +1
      erro_x = (display.y - tam_y/2)/(tam_y/2)
      
      integralx = integralx*INTEGRAL_VALUE + erro_x*ts
#        print "erro_x:", erro_x
#        print "last_erro_x:",last_erro_x
#        print "time:", ts
      derivative = (erro_x - last_erro_x)/ts
#      time.sleep(0.5)

      last_erro_x = erro_x        
      computedMovementx = Kpx*erro_x + Kix*integralx + Kdx*derivative
      computedMovementx = computedMovementx/SCALE
#
#      print "proporcional X = ", erro_x, "integral X= ", integralx, "derivativo X= ", derivative
#      print "movimento computado X", computedMovementx
#      print "proproriconal = ", -erro_x*Kvx

#----------- PARA Y ----------- 
      Kvy = 1	#Kv e o valor maximo de velocidade que sera aplicado para o valor maximo de erro       
      tam_y = 337	#depois, subistituir esse valor pela variavel de redimensionamento do frame, assim, quando oframe for redimensionado para melhorar a precisao, nao precisao nao precisa mduar essa variavel
      #Encontra o erro em x, que pode variar entre -1 e +1
      erro_y = (display.x - tam_x/2)/(tam_x/2)
      integraly = integraly*INTEGRAL_VALUE + erro_y*ts
#        print "erro_x:", erro_x
#        print "last_erro_x:",last_erro_x
#        print "time:", ts
      derivative = (erro_y - last_erro_y)/ts
      #time.sleep(0.5)

      last_erro_y = erro_y        
      computedMovementy = Kpy*erro_y + Kiy*integraly + Kdy*derivative
      computedMovementy = computedMovementy/SCALE

#      print "proporcional Y = ", erro_y, "integral Y= ", integraly, "derivativo Y= ", derivative
#      print "movimento computado Y", computedMovementy    
#      print "proproriconal = ", -erro_y*Kvy 

      #seta velocidade em funcao do erro e ganho
      #controller.command.linear.y = -erro_y*Kvx		#sinal negativo a ser definido dependendo da orientacao da coordenada local do drone
      controller.command.linear.y = -computedMovementy	#sinal negativo a ser definido dependendo da orientacao da coordenada local do drone
           
      #seta velocidade em funcao do erro e ganho
      #controller.command.linear.x = -erro_x*Kvy	#sinal negativo a ser definido dependendo da orientacao da coordenada local do drone
      controller.command.linear.x = -computedMovementx #sinal negativo a ser definido dependendo da orientacao da coordenada local do drone    
#      print "em Vx: ", -erro_y*Kvy
#      print "em Vy: ", -erro_x*Kvx
      
      #faz controle algular do drone
      
#      Kvphi = 0.03	#Kv e o valor maximo de velocidade que sera aplicado para o valor maximo de erro       
      #Encontra o erro em x, que pode variar entre -1 e +1
      if abs(erro_x) < 20 and abs(erro_y) < 20 :
          if display.phi < -90:
              erro_phi = -(270 + display.phi)
          else:
              erro_phi = 90 - display.phi      
              
          controller.command.angular.z = -erro_phi*Kvphi
          
      
      #Controle do carro      
      #Erro linear
      carro.x = display.erro_lin
      #Erro angular
      carro.theta = display.erro_ang              
      
      
      #publicando informacoes
      if abs(erro_x) > 20 and abs(erro_y) > 20 :
          print "erlin: ", round(carro.x,2), "erang: ", round(carro.theta,2)
          pub.publish(carro)
      else:
          carro.x = 0 
          carro.theta = 0
          pub.publish(carro)
            
#<--FIM Controle do voo do drone-->            
