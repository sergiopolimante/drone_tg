#!/usr/bin/env python
# license removed for brevity
import rospy
from geometry_msgs.msg import Pose2D

def talker():
    pub = rospy.Publisher('ToController', Pose2D, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    
    pose = Pose2D()
    pose.x = 0
    pose.y = 0
    pose.theta = 0
    pub.publish(pose)
    print "pose = ", pose
	


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
